#+TITLE: SWMN Summary
#+AUTHOR: Felipe Favelas

#+OPTIONS: H:5 date:nil
#+LATEX_HEADER: \setcounter{secnumdepth}{5}
#+LATEX_HEADER: \setcounter{tocdepth}{5}

\newpage
#+INCLUDE: "0-introduzione.org"
\newpage
#+INCLUDE: "1-crittografia.org"
\newpage
#+INCLUDE: "2-firewall.org"
\newpage
#+INCLUDE: "3-nat.org" 
\newpage
#+INCLUDE: "4-attacchi.org" 
\newpage
#+INCLUDE: "5-sicurezza_reti.org" 

