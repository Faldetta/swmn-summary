** Address Translation (NAT)

L'espanzione di internet ci ha portati verso una carenza di indirizzi
IP, che sono diventato conseguentemente costosi. È nata inoltre la
necessità di mascherare la struttura della rete interna a quella
esterna.

I *Network Address Translation* e i *Network Address Port Translation*
mascherano un indirizzo tramite un proxy a livello IP. In pratica
dall'esterno il server NAT viene visto come la sorgente della
comunicazione, in realtà modifica gli indirizzi (IP e porta) dei
pacchetti in transito.

In questo modo è possibile associare a ad un indirizzo IP pubblico
(quello del NAT) più indirizzi IP privati (o /non routable/),
appartenenti a una delle sequenti classi:

#+ATTR_LATEX: :align |c|c
| Classe | Address Range                 |
|--------+-------------------------------|
| A      | 10.0.0.0 - 10.255.255.255     |
| B      | 172.16.0.0 - 172.31.255.255   |
| C      | 192.168.0.0 - 192.168.255.255 |

In base a come viene effettuato il mapping tra indirizzi interni ed
esterni si parla di NAT

- statico :: effettua un mapping uno a uno tra indirizzi interni ed
  esterni; facile da implementare ma non aiuta con la scarsità di
  indirizzi.

o

- dinamico :: effettua un mappping uno a molti tra indirizzi interni ed
  esterni; è più complicato della variante statica, in quanto richiede
  un server stateful, ma risolve la scarsità di indirizzi.

Spesso insieme al NAT viene usato il protocollo IPsec, questo permette
di ottenere connessioni autenticate, cifrate e integre su reti IP (viene
infatti aggiunto un header a livelo 3). Possiamo vedere IPsec come una
collezione di protocolli atti ad impementare lo scambio delle chiavi
crittografiche e la cifratura del traffico.

Tuttavia IPsec genera qualche complicazione in funzione della posizione
relativa tra la sua applicazione ed il NAT. Nel caso in cui IPsec venga
decifrato, in corrispondenza o prima del NAT, si vanno a indebolire le
proprietà che IPsec ci fornisce in quanto a sicurezza; nel caso in cui
si provi a decifrare un pacchetto dopo che questo sia stato smistato dal
NAT sorge il problema che quest'ultimo non saprebbe indirizzare il
messaggio a causa della cifrature degli header. La soluzione è quella di
incapsulare il pacchetto cifrato dentro ad un'altro pacchetto; tale
tecnica prende il nome di IP-over-IP o tunneling.

Un'operazione fondamentale del NAT è la creazione dei /binding/, ovvero
le associazioni bidirezionali tra {IP, Protocollo, Porta}(interna)
\(\Longleftrightarrow\) {IP, Protocollo, Porta}(esterna). Normalmente i
binding vengono creati quando un messaggio inviato dall'interno non
corrisponde a nessuno di quelli presenti; vengono distrutti allo scadere
di un timer.

Per permettere la creazione di binding da un nuovo interlocutore esterno
è stato introdotto il /filter/, tuttavia filter diversi danno origine a
diversi comportamenti.

Il NAT si comporta in modo diverso anche in funzione del protocollo che
gestisce, nel caso di TCP i binding sono gestiti in funzione allo stato
della connessione (e relativi timer) e della finestra di congestione,
nel caso di UDP il comportamento varia in funzione del tipo di filter
applicato dal NAT:

- Symmetric NAT :: ogni richiesta proveniente da uno stesso IP/porta
  sorgente e diretta ad uno specifico IP/porta destinazione esterno è
  mappato in un'unico IP/porta sorgente esterno. Uno stesso host interno
  al NAT avrà tante mappatture differenti quanti sono i suoi
  interlocutori all'esterno del NAT.

- Full Cone NAT :: una volta che un indirizzo interno iAddr:iPort viene
  mappato in un indirizzo esterno eAddr:ePort, tutti i pacchetti
  provenienti da iAddr:iPort vengono inviati a eAddr:ePort. Ciò
  significa che il filter non fa nulla e che per comunicare con
  iAddr:iPort basta comunicare con eAddr:ePort.

- Address-Restricted Cone NAT :: come un Full Cone NAT ma con la
  seguente restrizione: un host esterno hAddr:any può inviare pacchetti
  a iAddr:iPort tramite eAddr:ePort solo se iAddr:iPort ha inviato prima
  un pacchetto verso hAddr:any.

- Port-Restricted Cone NAT :: come un Address-Restricted Cone NAT, ma le
  restrizioni includono anche il numero di porta: un host esterno
  hAddr:hPort può inviare pacchetti a iAddr:iPort tramite eAddr:ePort
  solo se iAddr:iPort ha inviato prima un pacchetto verso hAddr:hPort.

Se due host appartenenti alla stessa rete volessero comunicare, come
dovrebbero fare? Una soluzione ingenua è quella di far uscire il
pacchetto dal NAT per farlo rientrare subito, una soluzione interessante
è l'*hairpin* (o NAT loopback), che però non è supportata da tutti i
NAT; tale tecnica prevede l'uso da parte degli host di una mappa degli
endpoints.

Affinchè un host possa sapere che tipo di NAT è in uso, si avvale del
protocollo *STUN*. Tale protocollo, di tipo request-reply, cattura il
comportamento del NAT in un certo istante di tempo; questo comportamento
può in seguito variare (anche in base alle risorse disponibili).

Il comportamento del NAT varia anche in funzione del binding, ossia di
come può lavorare il server NAT in sè:

- Endpoint Independent :: il NAT riusa il binding per tutte le
  connessioni provenienti dallo stesso IP:porta, l'IP:porta esterno non
  è valutato; è come un /Full Cone NAT/.

- Endpoint Address Dependent :: il NAT riusa il binding per tutte le
  sessioni provenienti dallo stesso IP:porta verso lo stesso IP esterno,
  la porta esterna non è considerata; è come un /Restricted Cone NAT/.

- Endpoint Address and Port Dependent :: si utilizza la quintupla
  {protocollo, IP sorgente, porta sorgente, IP destinazione, porta
  destinazione}, ovvero si usa lo stesso mapping per tutti i pacchetti
  inviati ai medesimi IP:porta esterni; è come un /Symmetric NAT/.

- Endpoint Port Dependent :: il NAT riusa il binding per tutte le
  sessioni provenienti dallo stesso IP:porta verso la stessa porta
  esterna, l'IP esterno non è considerato; è come un /Port Restricted
  Cone NAT/.

Il port binding può essere suddiviso nelle seguenti categorie:

- Port Preservation :: il NAT può tentare di mantenere la porta di
  origine; nel caso due host abbiano la stessa porta di origine, una
  sarà cambiata e l'altra no.

- Port Overloading :: il NAT effettua una sorta di port preservation in
  modo aggressivo: un secondo tentativo di binding su una porta
  esistente fa scadere il binding esistente, ovvero la porta non viene
  cambiata all'ultimo host che ne fa richiesta.

- Port Multiplexing :: il NAT si occupa di fare (de)multiplexing
  cercando di mandare tutte le comunicazioni tramite un'unica porta;
  all'esterno i pacchetti appaiono provenienti dalla stesso IP:porta,
  sarà cura del NAT effettuare un demultiplexing corretto. Tuttavia, se
  due host volessero comunicare con lo stesso host:porta esterno,
  essendo il demultiplexing impossibile, uno dei due stream avrà una
  porta diversa. Nel caso di traffico elevato il comportamento diventa
  non deterministico.

Per quanto riguarda il timer abbiamo le quattro politiche di
aggiornamento:

- Bidirectional :: il timer viene aggiornato dai pacchetti che
  transitano in entrambi i sensi.

- Outbound :: il timer viene aggiornato solo dai pacchetti che
  transitano dall'interno verso l'esterno; ciò significa che se l'host
  deve solo ricevere dovra inviare periodicamente dei /keep-alive/.

- Inbound :: il timer viene aggiornato solo dai pacchetti che transitano
  dall'esterno verso l'interno; anche in questo caso possono essere
  necessari /keep-alive/.

- Transport Protocol State :: si effettua un'inferenza del tempo di vita
  del binding basandosi su informazioni del livello di transporto; è
  difficile da programmare ed eventuali bug potrebbero originare DoS.

Infine vediamo una classificazione per il filtering esterno:

- Endpoint Independent :: i pacchetti non vengono filtrati; è come un
  /Full Cone NAT/.

- Endpoint Address Dependent :: vengono filtrati i pacchetti che non
  provengono da IP di destinazione del binding; è come un /Restricted
  Cone NAT/.

- Endpoint Address and Port Dependent :: vengono filtrati i pacchetti
  che non provengono da IP:porta di destinazione del binding; è come un
  /Symmetric NAT/ o un /Port Restricted Cone NAT/.
